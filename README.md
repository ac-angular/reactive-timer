# ReactiveTimer

Implementación de temporizador por medio de programación reactiva

## Conceptos a desarrollar

BehaviorSubject, Subject, Observables, EventEmitter, Subscription

## Tecnologias

Lenguaje: Typescript

Framework: Angular (versión: 9.1.12)

## Uso

Para su uso ejecutar los siguientes comandos:

```bash
1) npm install
2) ng serve -o
```
## Referencias
[Enrique Oriol](http://blog.enriqueoriol.com/2017/05/comunicacion-servicio-componente-en-angular.html)
