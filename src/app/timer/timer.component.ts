import { Component, OnInit, Output, EventEmitter, OnDestroy, Input } from '@angular/core';
import { TimerService } from './timer.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.scss']
})
export class TimerComponent implements OnInit, OnDestroy {
  @Input() time: number = 0;
  @Output() state = new EventEmitter<string>();
  startSubscription = new Subscription();
  endSubscription = new Subscription();


  constructor(public timerService: TimerService) { }

  ngOnInit(): void {
    this.startSubscription = this.timerService.getStart().subscribe(data => {
      this.state.emit('start');
    })
    this.endSubscription = this.timerService.getEnd().subscribe(data => {
      this.state.emit('end');
    })
  }

  ngOnDestroy(): void {
    this.startSubscription.unsubscribe();
    this.endSubscription.unsubscribe();
  }

}
