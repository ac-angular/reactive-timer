import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TimerService implements OnDestroy {

  private countdownTimerRef = null;
  private count = new BehaviorSubject(0);
  private start = new Subject();
  private end = new Subject();

  constructor() { }

  restart(time) {
    this.count.next(time);
    this.start.next();
    this.countdown();
  }

  countdown() {
    if (this.count.getValue() > 0) {
      this.countdownTimerRef = setTimeout(() => {
        this.count.next(this.count.getValue() - 1);        
        this.countdown();
      }, 1000);
    } else {
      this.end.next();
    }
  }

  getCount() {
    return this.count;
  }

  getStart() {
    return this.start;
  }

  getEnd() {
    return this.end;
  }

  ngOnDestroy(): void {
    clearTimeout(this.countdownTimerRef);
  }
}
