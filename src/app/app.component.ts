import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  ngOnInit(): void {
    this.message.display = this.message.initial;
  }

  title = 'reactive timer';
  message = {
    initial: 'Carga un valor y presiona Comenzar',
    countStart: 'El conteo ha iniciado',
    countEnd: 'El conteo ha finalizado',
    display: '',
  };
  time = 0;

  onState(state) {
    switch (state) {
      case 'start':
        this.message.display = this.message.countStart;
        break;
      case 'end':
        this.message.display = this.message.countEnd;
        setTimeout(() => {
          this.message.display = this.message.initial;
          this.time = 0;
        }, 2000);
        break;
    }
  }

  isNumberKey(evt) {
    console.log(evt);
    
    var charCode = evt.which ? evt.which : evt.keyCode;
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
  }
}
